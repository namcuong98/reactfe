import { BrowserRouter, Route, Router, Routes } from "react-router-dom";
import Login from "./pages/login/Index";
import Home from "./pages/home/Index";
import Layout from "./components/Layout";
import Products from "./pages/product/Index";
import NotFound from "./pages/404";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="*" element={<NotFound />} />
          <Route path="/" element={<Layout />}>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products />} />
            <Route path="/nganh-nghe" element={<Products />} />
          </Route>
          <Route path="/login" element={<Login />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
