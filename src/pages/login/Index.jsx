import React, { useState } from "react";
import { apiInstance } from "../../utils/api";
import { useNavigate } from "react-router-dom";
import topImages from "../../asset/imanges/Ảnh 1.png";
import botImages from "../../asset/imanges/Ảnh 2.png";

export default function Login() {
  const navigate = useNavigate();
  const [account, setAccount] = useState({ useName: "", usePassword: "" });

  function handleChange(e) {
    const { name, value } = e.target;
    setAccount((prevValue) => {
      return {
        ...prevValue,
        [name]: value,
      };
    });
  }

  function handleLogin() {
    // e.preventDefault();
    apiInstance({
      url: "/api/auth/login",
      method: "POST",
      params: {
        username: account.useName,
        password: account.usePassword,
      },
    }).then((response) => {
      const responseData = response.data;
      const { token, userId } = responseData;
      localStorage.setItem("token", token);
      localStorage.setItem("userId", userId);
      if (token === null) {
        alert("Sai tài khoản hoặc mật khẩu");
      } else if (token !== null) {
        navigate("/products");
      }
      console.log(responseData);
    });
  }

  return (
    <>
      <div className="w-full grid grid-cols-12">
        <div className="col-span-3 font-bold m-5">Quản Lý Đồ Án</div>
        <div className="col-span-4 flex justify-center">
          <div className="text-center">
            <div className="text-sky-500 font-bold text-2xl	p-9 mt-[50px]">
              Đăng nhập
            </div>
            <div>
              <form className="">
                <p className="text-start mb-1">Tài khoản</p>
                <input
                  value={account.useName}
                  onChange={handleChange}
                  name="useName"
                  className="border-2 border-stone-200 rounded-md p-1 pl-5 block w-[300px] mb-5"
                  type="text"
                  placeholder="Tài khoản"
                />
                <p className="text-start mb-1">Mật khẩu</p>
                <input
                  value={account.usePassword}
                  onChange={handleChange}
                  name="usePassword"
                  className="border-2 border-stone-200 rounded-md p-1 pl-5 block w-[300px] mb-10"
                  type="password"
                  placeholder="Mật khẩu"
                />
              </form>
              <button
                className="w-full text-white rounded-md p-2 
                transition ease-in-out delay-75 bg-blue-400 hover:-translate-y-1 hover:scale-110 hover:bg-indigo-400 duration-300"
                onClick={handleLogin}
              >
                Đăng nhập
              </button>
            </div>
          </div>
        </div>
        <div className="col-span-5 relative">
          <div className="absolute h-screen top-0 right-0">
            <img className="w-full max-h-[376px]" src={topImages} alt="" />
            <img className="w-full max-h-[376px]" src={botImages} alt="" />
          </div>
        </div>
      </div>
    </>
  );
}
