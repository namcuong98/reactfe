import { useEffect, useState } from "react";
import axios from "axios";

export default function Products() {
  const [data, setData] = useState([]);

  //wating function call api done continue loading page web
  const getData = async () => {
    const response = await axios({
      url: "https://fakestoreapi.com/products",
      method: "GET",
    });
    setData(response.data);
  };

  // inside useEffect is call function. not biểu thức
  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <div>products</div>
      {data.map((item, index) => {
        return (
          <div className="">
            <p>{item.id}</p>
            <p>{item.title}</p>
            <p>{item.price}</p>
            <div className="max-w-[200px]">
              <img src={item.image} alt="img" />
            </div>
          </div>
        );
      })}
    </>
  );
}
