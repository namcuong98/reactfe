import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { apiLoggedInstance } from "../../utils/api";

export default function Home() {
  const navigate = useNavigate();

  const token = localStorage.getItem("token");
  const [user, setUser] = useState({});

  useEffect(() => {
    if (token) {
      apiLoggedInstance(token)({
        url: "/api/auth/user-info",
        method: "GET",
      }).then((response) => {
        setUser(response.data);
        console.log(response);
      });
    }
  }, [token]);

  return (
    <>
      <div>
        <button
          onClick={() => {
            navigate("/login");
          }}
        >
          Đăng nhập
        </button>
        <div>{token !== "null" ? "Đã đăng nhập" : "Chưa đăng nhập"}</div>
      </div>
    </>
  );
}
