import axios from "axios";

export const apiInstance = axios.create({
  baseURL: "https://training.bks.center/",
});

export const apiLoggedInstance = (token) =>
  axios.create({
    baseURL: "https://training.bks.center/",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
