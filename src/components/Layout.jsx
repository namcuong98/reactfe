import { useState } from "react";
import { Link, Outlet, useLocation } from "react-router-dom";

export default function Layout() {
  const menus = [
    {
      path: "/",
      name: "Home",
    },
    {
      path: "/products",
      name: "Quản trị thành viên",
    },
    {
      path: "/danh-muc",
      name: "Danh mục",
      child: [
        {
          path: "/nganh-nghe",
          name: "Ngành nghề",
        },
      ],
    },
  ];

  return (
    <div className="flex overflow-y-hidden w-full h-screen">
      <div className="h-full w-1/4 border-2 border-r-black">
        {menus.map((menu, index) => {
          return (
            <MenuItem
              path={menu.path}
              name={menu.name}
              child={menu.child}
              key={index}
            />
          );
        })}
      </div>
      <div className="overflow-y-auto w-3/4 h-full">
        <div className="flex justify-between items-center py-5 px-7 border-b-2 border-black">
          <div className=" text-xl font-w font-semibold	text-sky-400">
            Trường đại học A
          </div>
          <div className="flex gap-3 text-xl items-center">
            <i className="cursor-pointer hover:-translate-y-1 hover:scale-110 fa-solid fa-flag"></i>
            <i className="cursor-pointer hover:-translate-y-1 hover:scale-110 fa-solid fa-bell"></i>
            <i className="cursor-pointer hover:-translate-y-1 hover:scale-110 fa-solid fa-user-tie"></i>
          </div>
        </div>
        <Outlet />
      </div>
    </div>
  );
}

const MenuItem = ({ path, name, child }) => {
  const { pathname } = useLocation();
  const [openChild, setOpenChild] = useState(false);

  // if not "child: {}" in element array menus
  if (!child) {
    return (
      <Link to={path}>
        <div className={pathname === path ? "text-sky-400 font-semibold" : ""}>
          {name}
        </div>
      </Link>
    );
  } else {
    // if "child: {}" in element array menus
    return (
      <>
        <div
          onClick={() => {
            setOpenChild(!openChild);
          }}
        >
          {name}
        </div>
        {openChild &&
          child.map((item, index) => (
            <Link to={item.path} key={index}>
              <div
                className={
                  pathname === item.path ? "text-sky-400 font-semibold" : ""
                }
              >
                {item.name}
              </div>
            </Link>
          ))}
      </>
    );
  }
};
